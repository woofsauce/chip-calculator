"""Some data types every script need"""
from dataclasses import dataclass, replace
import collections
import itertools
import math
import random
import string
import typing

from pyparsing import Dict, Group, Literal, OneOrMore, Optional, Word, ZeroOrMore, restOfLine

@dataclass
class Mask:
    """Shape of a chip or an area i.e. a collection of blocks on 2D grid"""
    mask: typing.AbstractSet[typing.Tuple[int, int]]

    _CLASS1 = []

    def __init__(self, mask):
        if isinstance(mask, str):
            mask = ((i, j) for i, line in enumerate(mask.split('/'))
                    for j, c in enumerate(line.strip())
                    if c == 'x')
        self.mask = frozenset(mask)

    def width(self):
        """Width of this mask"""
        return max((c + 1 for r, c in self.mask), default=0)

    def height(self):
        """Height of this mask"""
        return max((r + 1 for r, c in self.mask), default=0)

    def shift_right(self, n=1):
        """Shift this chip right by n blocks"""
        return Mask((r, c + n) for r, c in self.mask)

    def shift_down(self, n=1):
        """Shift this chip down by n blocks"""
        return Mask((r + n, c) for r, c in self.mask)

    def translations(self, maxh, maxw):
        """Return all translations of this mask"""
        for i in range(maxh - self.height() + 1):
            for j in range(maxw - self.width() + 1):
                yield self.shift_down(i).shift_right(j)

    def rotate_clockwise(self):
        """Rotate this chip clockwise"""
        height = self.height()
        return Mask((c, height - 1 - r) for r, c in self.mask)

    def rotations(self):
        """Return all unique rotations of this mask"""
        yield self
        mask = self
        while True:
            mask = mask.rotate_clockwise()
            if mask == self:
                break
            yield mask

    @staticmethod
    def _gen_class1():
        return [mask for pmask in [
            Mask('.xx/xx/.x'),
            Mask('xx/.xx/.x'),
            Mask('xxx/..xx'),
            Mask('.xxx/xx'),
            Mask('x/xxx/x'),
            Mask('..x/.xx/xx'),
            Mask('.x/xxx/.x'),
            Mask('..x/xxxx'),
            Mask('.x/xxxx'),
        ] for mask in pmask.rotations()]

    @staticmethod
    def _get_class1():
        if not Mask._CLASS1:
            Mask._CLASS1 = Mask._gen_class1()
        return Mask._CLASS1

    @staticmethod
    def _density_table(stars):
        if stars == 5:
            return [0.76, 0.8, 0.92, 1.0]
        if stars == 4:
            return [0.6, 0.64, 0.72, 0.8]
        if stars == 3:
            return [0.48, 0.48, 0.56, 0.6]
        if stars == 2:
            return [0.32, 0.32, 0.36, 0.4]
        raise ValueError(f'Invalid stars: {stars}')

    def density(self, stars):
        """Get density of this mask"""
        table = self._density_table(stars)
        blocks = 6 if self in Mask._get_class1() else len(self)
        try:
            return table[blocks - 3]
        except IndexError:
            raise ValueError(f'Invalid blocks: {blocks}')

    def __len__(self):
        return len(self.mask)

    def __contains__(self, other):
        return other in self.mask

    def isdisjoint(self, other):
        """Returns ``True`` if if the mask has no blocks in common with *other*."""
        return self.mask.isdisjoint(other.mask)

    def issubset(self, other):
        """Test whether every blocks in the mask is in *other*"""
        return self.mask.issubset(other.mask)

    def __le__(self, other):
        return self.mask <= other.mask

    def __lt__(self, other):
        return self.mask < other.mask

    def issuperset(self, other):
        """Test whether every blocks in *other* is in the mask"""
        return self.mask.issuperset(other.mask)

    def __ge__(self, other):
        return self.mask >= other.mask

    def __gt__(self, other):
        return self.mask > other.mask

    def __iter__(self):
        return iter(self.mask)

    def _repr_row(self, row):
        return ''.join('x' if (row, col) in self else '.'
                       for col in range(self.width())).rstrip('.')

    def __str__(self):
        return '/'.join(self._repr_row(row) for row in range(self.height()))

    def __repr__(self):
        return f'{self.__class__.__name__}({str(self)!r})'

@dataclass
class Chip:
    """A chip, which has a mask and some stats"""
    mask: Mask
    d: int = 0
    p: int = 0
    a: int = 0
    r: int = 0
    t: int = 0

    def __len__(self):
        return len(self.mask)

    def __str__(self):
        ret = str(self.mask)
        ret += f' d={self.d}' if self.d else ''
        ret += f' p={self.p}' if self.p else ''
        ret += f' a={self.a}' if self.a else ''
        ret += f' r={self.r}' if self.r else ''
        ret += f' t={self.t}' if self.t else ''
        return ret

    @staticmethod
    def compute_value(stat, blocks, density, level):
        """Compute stat value of a chip """
        basevalue = math.ceil(stat * blocks * density)
        multiplier = (1.0 + level * 0.08 if level <= 10 else
                      1.1 + level * 0.07)
        return math.ceil(basevalue * multiplier)

    @staticmethod
    def compute_blocks(stat, value, density, level):
        """Compute stat blocks of a chip"""
        for blocks in itertools.count():
            test = Chip.compute_value(stat, blocks, density, level)
            if test == value:
                return blocks
            if test > value:
                raise ValueError('No satisfying blocks')
        raise AssertionError('itertools.count() terminated')

    def to_value(self, stars, level):
        """Get value version of this chip"""
        density = self.mask.density(stars)

        return replace(self,
                       d=Chip.compute_value(4.4, self.d, density, level),
                       p=Chip.compute_value(12.7, self.p, density, level),
                       a=Chip.compute_value(7.1, self.a, density, level),
                       r=Chip.compute_value(5.7, self.r, density, level))

    def to_blocks(self, stars):
        """Get blocks version of this chip"""
        density = self.mask.density(stars)

        for level in range(21):
            try:
                dmg = Chip.compute_blocks(4.4, self.d, density, level)
                pen = Chip.compute_blocks(12.7, self.p, density, level)
                acc = Chip.compute_blocks(7.1, self.a, density, level)
                rel = Chip.compute_blocks(5.7, self.r, density, level)
            except ValueError:
                continue
            if dmg + pen + acc + rel == len(self.mask):
                return replace(self, d=dmg, p=pen, a=acc, r=rel)

        raise ValueError('No satisfying blocks')

@dataclass
class Parameter:
    """Area, chips and other parameters"""
    area: Mask
    chips: [Chip]
    minc: int
    mind: int
    minp: int
    mina: int
    minr: int
    maxt: int

    _CONFIG_BNF = None

    @staticmethod
    def _gen_config_bnf():
        lbrack = Literal('[').suppress()
        rbrack = Literal(']').suppress()
        equals = Literal('=').suppress()
        word = Word(string.ascii_letters + string.digits + '-')

        area_def = lbrack + Literal('area') + rbrack
        area_section = OneOrMore(Word('.x'))

        param = Group(word + equals + word)
        param_def = lbrack + Literal('param') + rbrack
        param_section = Dict(OneOrMore(param))

        chip = Group(Word('.x/') + Group(Dict(ZeroOrMore(param))))
        chip_def = lbrack + word + rbrack
        chip_section = Group(ZeroOrMore(chip))

        bnf = Dict(OneOrMore(Group((area_def + area_section) |
                                   (param_def + param_section) |
                                   (chip_def + chip_section))))

        bnf.ignore(Literal(';') + Optional(restOfLine))
        return bnf

    @staticmethod
    def _get_config_bnf():
        if Parameter._CONFIG_BNF is None:
            Parameter._CONFIG_BNF = Parameter._gen_config_bnf()
        return Parameter._CONFIG_BNF

    @staticmethod
    def _parse_chip(chip_config):
        mask, attrs = chip_config
        mask = Mask(mask)
        if not isinstance(attrs, collections.Mapping):
            attrs = attrs.asDict()

        return Chip(mask,
                    d=int(attrs.get('d', 0)),
                    p=int(attrs.get('p', 0)),
                    a=int(attrs.get('a', 0)),
                    r=int(attrs.get('r', 0)))

    @staticmethod
    def parse(param_text):
        """Parse the parameter from *param_text*"""
        bnf = Parameter._get_config_bnf()
        config = bnf.parseString(param_text, parseAll=True)
        area = Mask('/'.join(config['area'].asList()))

        param = config['param']
        color = param['color']
        minc = len(area) if param['minc'] == 'FULL' else int(param['minc'])
        mind = int(param['mind'])
        minp = int(param['minp'])
        mina = int(param['mina'])
        minr = int(param['minr'])
        maxt = int(param['maxt'])

        chips = [Parameter._parse_chip(ccfg) for ccfg in config[color + '-chips']]

        return Parameter(area=area, chips=chips, minc=minc,
                         mind=mind, minp=minp, mina=mina, minr=minr,
                         maxt=maxt)

    @staticmethod
    def _find_non_substr(text):
        while True:
            test = ''.join(random.choices(string.ascii_letters, k=10))
            if test not in text:
                return test

    @staticmethod
    def parse_chips(param_text, omit_section=False):
        """Parse a list of chips"""
        if omit_section:
            default_sec = Parameter._find_non_substr(param_text) + '-chips'
            param_text = f'[{default_sec}]\n' + param_text
        else:
            default_sec = ''

        bnf = Parameter._get_config_bnf()
        config = bnf.parseString(param_text, parseAll=True)

        allchips = {}
        for section, chip_conf in config.asDict().items():
            if section.endswith('-chips'):
                chips = [Parameter._parse_chip(line) for line in chip_conf]

                if section == default_sec:
                    section = ''
                allchips[section] = chips

        return allchips

@dataclass
class Solution:
    """A solution i.e. some (translated and/or rotated) chips"""
    chips: typing.Dict[int, Chip]

    def sumc(self):
        """Total number of blocks"""
        return sum(len(chip) for chip in self.chips.values())
    def sumd(self):
        """Total damage"""
        return sum(chip.d for chip in self.chips.values())
    def sump(self):
        """Total penetration"""
        return sum(chip.p for chip in self.chips.values())
    def suma(self):
        """Total precision"""
        return sum(chip.a for chip in self.chips.values())
    def sumr(self):
        """Total reload"""
        return sum(chip.r for chip in self.chips.values())
    def sumt(self):
        """Total calibration tickets"""
        return sum(chip.t for chip in self.chips.values())

    def key(self):
        """same *key()* if and only if the same set of chips"""
        return frozenset(self.chips.keys())
