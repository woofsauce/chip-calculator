#!/usr/bin/env python3
"""Find and display solutions"""
from dataclasses import replace
from functools import reduce
import multiprocessing
import os.path
import sys
import threading
import traceback

from ortools.sat.python import cp_model
import ortools
import pyparsing
import wx
import wx.grid
import wx.lib.mixins.gridlabelrenderer as wxglr

from chiplib import Parameter, Solution

class SolutionTableBase(wx.grid.GridTableBase):
    """Provide Grid data from a list of solutions"""
    # 7 columns: chips, c, d, p, a, r, t

    def __init__(self, solutions=None):
        super().__init__()
        if solutions is None:
            solutions = []
        self._list = solutions

    def append(self, solution):
        """Append a solution"""
        self._list.append(solution)

        with wx.grid.GridUpdateLocker(self.View):
            msg = wx.grid.GridTableMessage(
                self, wx.grid.GRIDTABLE_NOTIFY_ROWS_APPENDED, 1)
            self.View.ProcessTableMessage(msg)

    def GetNumberRows(self):
        """Get the number of rows, which equals to the number of solutions"""
        return len(self._list)

    def GetNumberCols(self):
        """Get the number of rows, which equals to 7"""
        return 7

    def GetColLabelValue(self, col):
        """Get the label of column"""
        return ('Solution', 'Blocks', 'Damage', 'Penetration',
                'Accuracy', 'Reload', 'Cal. Tickets')[col]

    def GetValue(self, row, col):
        """Get the value of a cell"""
        sol = self._list[row]
        if col == 0:
            return ', '.join(str(k) for k in sol.chips)
        if 1 <= col <= 6:
            # sumc, sumd, sump, suma, sumr, sumt
            return str(getattr(sol, 'sum' + 'cdpart'[col - 1])())
        raise IndexError

    def sort(self, col):
        """Sort by a column"""
        reverse = False
        if col == 0:
            key = lambda sol: list(sol.chips)
        elif col == 1:
            reverse = True
            key = lambda sol: sol.sumc()
        elif col == 2:
            reverse = True
            key = lambda sol: sol.sumd()
        elif col == 3:
            reverse = True
            key = lambda sol: sol.sump()
        elif col == 4:
            reverse = True
            key = lambda sol: sol.suma()
        elif col == 5:
            reverse = True
            key = lambda sol: sol.sumr()
        elif col == 6:
            key = lambda sol: sol.sumt()

        self._list.sort(key=key, reverse=reverse)

    def GetAttr(self, _row, col, _kind):
        """Get the attribute of a cell"""
        attr = wx.grid.GridCellAttr()
        attr.SetReadOnly(True)
        if 1 <= col <= 6:
            attr.SetAlignment(wx.ALIGN_RIGHT, wx.ALIGN_TOP)
        return attr

    def __getitem__(self, index):
        return self._list[index]

class SolutionCallback(cp_model.CpSolverSolutionCallback):
    """Solution callback"""
    def __init__(self, conn, area, chips):
        super().__init__()
        self._conn = conn
        self._area = area
        self._chips = chips
        self._solutions = {}

    def on_solution_callback(self):
        """This is called by the solver"""
        chips = {}
        for var, chip in self._chips:
            if self.SolutionBooleanValue(var.Index()):
                chipid = int(str(var)[1 : str(var).find('_')])
                chips[chipid] = chip
        solution = Solution(chips)

        key = solution.key()
        oldsol = self._solutions.get(key)
        if oldsol and oldsol.sumt() <= solution.sumt():
            return
        self._solutions[key] = solution

        self._conn.send(solution)

class ResultEvent(wx.PyEvent):
    """Event that hold arbitrary data"""
    ID = wx.ID_ANY

    def __init__(self, data):
        super().__init__(eventType=ResultEvent.ID)
        self.data = data

class ChipColLabelRenderer(wxglr.GridDefaultColLabelRenderer):
    """Render stats and tickets with icons"""
    IMAGES = {
        'Damage': os.path.join('icons', 'd.png'),
        'Penetration': os.path.join('icons', 'p.png'),
        'Accuracy': os.path.join('icons', 'a.png'),
        'Reload': os.path.join('icons', 'r.png'),
        'Cal. Tickets': os.path.join('icons', 't.png'),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._bitmaps = {}
        for name, path in self.IMAGES.items():
            bitmap = wx.Bitmap(path)
            if bitmap.IsOk():
                self._bitmaps[name] = bitmap

    def _draw_bitmap(self, grid, dc, rect, col):
        if not dc.CanDrawBitmap():
            return False

        bitmap = self._bitmaps.get(grid.GetColLabelValue(col))
        if bitmap is None:
            return False

        self.DrawBorder(grid, dc, rect)
        dc.DrawBitmap(bitmap, rect.topLeft)
        return True

    def Draw(self, grid, dc, rect, col):
        result = self._draw_bitmap(grid, dc, rect, col)
        if not result:
            super().Draw(grid, dc, rect, col)

class ChipGrid(wx.grid.Grid, wxglr.GridWithLabelRenderersMixin):
    """A grid that renders stats and tickets with icons"""
    def __init__(self, *args, **kwargs):
        wx.grid.Grid.__init__(self, *args, **kwargs)
        wxglr.GridWithLabelRenderersMixin.__init__(self)

        self.SetDefaultColLabelRenderer(ChipColLabelRenderer())
        self.ColLabelSize = 36
        self.DefaultColSize = 35

class ResultFrame(wx.Frame):
    """The primary frame of this app, showing found solutions"""
    COLORS = ['RED', 'ORANGE', 'YELLOW', 'GREEN',
              'CYAN', 'BLUE', 'PURPLE', 'MAGENTA']

    def __init__(self, area, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._make_menu_bar()

        self._area = area
        self._base = SolutionTableBase()

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        self._grid = ChipGrid(self)
        sizer.Add(self._grid, 2, wx.EXPAND, 0)
        self._grid.SetTable(self._base, selmode=wx.grid.Grid.SelectRows)
        self._grid.MinSize = 500, 500
        self._grid.AutoSizeColumn(0)
        self._grid.AutoSizeColumn(1)
        self.Bind(wx.grid.EVT_GRID_SELECT_CELL, self._on_grid_select_cell, self._grid)
        self.Bind(wx.grid.EVT_GRID_COL_SORT, self._on_grid_col_sort, self._grid)

        sizer.Add(self._create_right_panel(), 0, wx.EXPAND, 0)

        sizer.SetSizeHints(self)
        self.Sizer = sizer

    def _create_right_panel(self):
        sizer = wx.BoxSizer(wx.VERTICAL)

        self._status = wx.StaticText(self, label="No solutions found yet")
        sizer.Add(self._status, 0, wx.EXPAND, 0)

        self._gauge = wx.Gauge(self)
        sizer.Add(self._gauge, 0, wx.EXPAND, 0)

        self._bitmap = wx.StaticBitmap(self)
        sizer.Add(self._bitmap, 0, 0, 0)
        self._update_bitmap(None)

        self._grid2 = ChipGrid(self)
        sizer.Add(self._grid2, 1, wx.EXPAND, 0)
        self._grid2.CreateGrid(0, 6)
        self._grid2.MinSize = 212, 252
        self._grid2.DisableCellEditControl()
        self._grid2.EnableEditing(False)
        self._grid2.HideRowLabels()
        self._grid2.SetDefaultCellAlignment(wx.ALIGN_RIGHT, wx.ALIGN_TOP)
        for col, value in enumerate(
                ('ID', 'Damage', 'Penetration',
                 'Accuracy', 'Reload', 'Cal. Tickets')):
            self._grid2.SetColLabelValue(col, value)

        return sizer

    def _make_menu_bar(self):
        file_menu = wx.Menu()
        open_config_item = file_menu.Append(-1, "Open Config")
        file_menu.AppendSeparator()
        exit_item = file_menu.Append(wx.ID_EXIT)

        help_menu = wx.Menu()
        about_item = help_menu.Append(wx.ID_ABOUT)

        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, "&File")
        menu_bar.Append(help_menu, "&Help")
        self.MenuBar = menu_bar

        self.Bind(wx.EVT_MENU, self._on_open_config, open_config_item)
        self.Bind(wx.EVT_MENU, self._on_exit, exit_item)
        self.Bind(wx.EVT_MENU, self._on_about, about_item)
        self.Connect(-1, -1, ResultEvent.ID, self._on_result)

    def _update_bitmap(self, solution, bsz=26):
        bitmap = wx.Bitmap(self._area.width() * bsz + 1,
                           self._area.height() * bsz + 1)
        dc = wx.MemoryDC()
        dc.SelectObject(bitmap)
        self._draw_solution(dc, solution, bsz)
        dc.SelectObject(wx.NullBitmap)
        self._bitmap.Bitmap = bitmap

    def _draw_solution(self, dc, solution, bsz):
        bg_color = self.BackgroundColour
        dc.Background = wx.TheBrushList.FindOrCreateBrush(bg_color)
        dc.Clear()

        for row, col in self._area:
            dc.DrawRectangle(col * bsz, row * bsz, bsz + 1, bsz + 1)

        if solution:
            for i, chip in enumerate(solution.chips.values()):
                self._draw_chip(dc, i, chip, bsz)

    def _draw_chip(self, dc, i, chip, bsz):
        color_name = self.COLORS[i % len(self.COLORS)]
        bg_color = wx.TheColourDatabase.Find(color_name)
        pen = wx.ThePenList.FindOrCreatePen(bg_color)
        pen.Cap = wx.CAP_PROJECTING
        dc.Pen = pen
        dc.Brush = wx.TheBrushList.FindOrCreateBrush(bg_color)
        for row, col in chip.mask:
            dc.DrawRectangle(col * bsz + 1, row * bsz + 1, bsz - 1, bsz - 1)
            if (row, col + 1) in chip.mask:
                dc.DrawLine((col + 1) * bsz, row * bsz + 1, (col + 1) * bsz, (row + 1) * bsz - 1)
            if (row + 1, col) in chip.mask:
                dc.DrawLine(col * bsz + 1, (row + 1) * bsz, (col + 1) * bsz - 1, (row + 1) * bsz)

    def _update_grid2(self, solution):
        n = len(solution.chips)
        delta = n - self._grid2.NumberRows

        with wx.grid.GridUpdateLocker(self._grid2):
            if delta > 0:
                self._grid2.AppendRows(delta)
            elif delta < 0:
                self._grid2.DeleteRows(n, -delta)

            for row, (chipid, chip) in enumerate(solution.chips.items()):
                values = [chipid, chip.d, chip.p, chip.a, chip.r, chip.t]
                for col, value in enumerate(values):
                    self._grid2.SetCellValue(row, col, str(value))

                color_name = self.COLORS[row % len(self.COLORS)]
                bg_color = wx.TheColourDatabase.Find(color_name)
                self._grid2.SetCellBackgroundColour(row, 0, bg_color)

    def _on_grid_select_cell(self, _event):
        rows = self._grid.SelectedRows
        if not rows:
            return
        solution = self._base[rows[0]]
        self._update_bitmap(solution)
        self._update_grid2(solution)

    def _on_grid_col_sort(self, event):
        self._base.sort(event.Col)

    def _on_open_config(self, _event):
        wx.LaunchDefaultApplication('param.txt')

    def _on_exit(self, _event):
        self.Close(True)

    def _on_about(self, _event):
        wx.MessageBox("Copyright \u00a9 2019 Pochang Chen\n\n"
                      "This software is under the MIT License.\n"
                      "See LICENSE.txt for details.\n\n"
                      "Library versions:\n"
                      f"OR-Tools {ortools.__version__}\n"
                      f"PyParsing {pyparsing.__version__}\n"
                      f"wxPython {wx.__version__}",
                      "About Chip Calculator",
                      wx.OK | wx.ICON_INFORMATION)

    def _on_result(self, event):
        data = event.data
        if isinstance(data, Solution):
            self._base.append(data)
            n = self._grid.NumberRows
            if n <= 100:
                self._grid.AutoSizeColumn(0)
                self._grid.AutoSizeColumn(1)
            if n == 1 or len(str(n)) > len(str(n - 1)):
                self._grid.RowLabelSize = wx.grid.GRID_AUTOSIZE
            self._status.Label = f'{n} solution{"s" if n != 1 else ""} found'
            self._gauge.Pulse()
        else:
            n = self._grid.NumberRows
            if n == 0:
                self._status.Label = f'No solutions'
            self._gauge.Value = self._gauge.Range

class WorkerProcess(multiprocessing.Process):
    """A process that do the actual work"""
    def __init__(self, conn, param):
        super().__init__()
        self._conn = conn
        self._param = param

    def run(self):
        model, chips = create_model(self._param)
        solver = cp_model.CpSolver()
        callback = SolutionCallback(self._conn, self._param.area, chips)
        solver.SearchForAllSolutions(model, callback)
        self._conn.close()

class ResultReceivingThread(threading.Thread):
    """Thread receiving results and sending events to window"""
    def __init__(self, conn, window, process):
        super().__init__()
        self._conn = conn
        self._window = window
        self._process = process

    def run(self):
        try:
            while True:
                wx.PostEvent(self._window, ResultEvent(self._conn.recv()))
        except EOFError:
            pass
        wx.PostEvent(self._window, ResultEvent(None))
        self._process.join()

def create_model(param):
    """Create a model from the parameter"""
    area = param.area

    model = cp_model.CpModel()

    chips = [] # [(var, chip)]

    for i, chip in enumerate(param.chips):
        variants = []

        for n_rotate, mask2 in enumerate(chip.mask.rotations()):
            for j, mask3 in enumerate(mask2.translations(maxh=area.height(),
                                                         maxw=area.width())):
                if not area.issuperset(mask3):
                    continue
                if n_rotate:
                    chip3 = replace(chip, mask=mask3, t=50)
                else:
                    chip3 = replace(chip, mask=mask3)

                var = model.NewBoolVar(f'x{i}_{n_rotate}_{j}')
                chips.append((var, chip3))
                variants.append(var)

        model.AddSumConstraint(variants, 0, 1)

    for pos in area:
        elemvars = [var for var, chip in chips if pos in chip.mask]
        model.AddSumConstraint(elemvars, 0, 1)

    model.Add(reduce(lambda x, y: x + y,
                     (var * len(chip) for var, chip in chips))
              >= param.minc)

    model.Add(reduce(lambda x, y: x + y,
                     (var * chip.d for var, chip in chips)) >= param.mind)
    model.Add(reduce(lambda x, y: x + y,
                     (var * chip.p for var, chip in chips)) >= param.minp)
    model.Add(reduce(lambda x, y: x + y,
                     (var * chip.a for var, chip in chips)) >= param.mina)
    model.Add(reduce(lambda x, y: x + y,
                     (var * chip.r for var, chip in chips)) >= param.minr)
    model.Add(reduce(lambda x, y: x + y,
                     (var * chip.t for var, chip in chips)) <= param.maxt)

    return model, chips

def main():
    """Just the main function"""
    try:
        param_text = open('param.txt', encoding='utf-8-sig').read()
    except OSError as exc:
        app = wx.App()
        wx.MessageBox(str(exc), 'Cannot open param.txt', style=wx.ICON_ERROR)
        sys.exit(1)

    try:
        param = Parameter.parse(param_text)
    except pyparsing.ParseBaseException as exc:
        app = wx.App()
        wx.MessageBox(str(exc), f'Cannot parse param.txt', style=wx.ICON_ERROR)
        sys.exit(1)
    except Exception as exc:
        app = wx.App()
        error_text = traceback.format_exc()
        wx.MessageBox(error_text, f'Cannot parse param.txt', style=wx.ICON_ERROR)
        sys.exit(1)

    conn1, conn2 = multiprocessing.Pipe(False)
    process = WorkerProcess(conn2, param)
    process.start()
    conn2.close()

    app = wx.App()
    frame = ResultFrame(param.area, None, title="Chip Calculator")

    thread = ResultReceivingThread(conn1, frame, process)
    thread.daemon = True
    thread.start()

    frame.Show()
    app.MainLoop()

    process.terminate()

if __name__ == '__main__':
    main()
