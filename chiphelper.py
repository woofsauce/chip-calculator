#!/usr/bin/env python3
"""An app that helps you record chips"""
import os.path

import openpyxl
import wx

from chiplib import Mask, Chip, Parameter

_ICON_PATHES = [
    os.path.join('icons', 'd.png'),
    os.path.join('icons', 'p.png'),
    os.path.join('icons', 'a.png'),
    os.path.join('icons', 'r.png'),
]

class ChipHelperFrame(wx.Frame):
    """Main frame of Chip Helper"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._make_menu_bar()

        sizer = wx.BoxSizer(wx.VERTICAL)

        notebook = wx.Notebook(self)
        sizer.Add(notebook)
        if wx.PlatformInformation.Get().PortId != wx.PORT_MSW:
            notebook.BackgroundColour = self.BackgroundColour

        easy_mode_tab = EasyModePanel(notebook)
        notebook.AddPage(easy_mode_tab, "Easy Mode")

        import_hycdes_tab = ImportHycdesPanel(notebook)
        notebook.AddPage(import_hycdes_tab, "Import from hycdes.com")

        import_excel_tab = ImportExcelPanel(notebook)
        notebook.AddPage(import_excel_tab, "Import from Excel")

        strengthen_tab = StrengthenPanel(notebook)
        notebook.AddPage(strengthen_tab, "Strengthen Chips")

        sizer.SetSizeHints(self)
        self.Sizer = sizer

    def _make_menu_bar(self):
        file_menu = wx.Menu()
        exit_item = file_menu.Append(wx.ID_EXIT)

        help_menu = wx.Menu()
        about_item = help_menu.Append(wx.ID_ABOUT)

        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, "&File")
        menu_bar.Append(help_menu, "&Help")
        self.MenuBar = menu_bar

        self.Bind(wx.EVT_MENU, self._on_exit, exit_item)
        self.Bind(wx.EVT_MENU, self._on_about, about_item)

    def _on_exit(self, _event):
        self.Close(True)

    @staticmethod
    def _on_about(_event):
        wx.MessageBox("Copyright \u00a9 2019 Pochang Chen\n\n"
                      "This software is under the MIT License.\n"
                      "See LICENSE.txt for details.\n\n"
                      "Library versions:\n"
                      f"openpyxl {openpyxl.__version__}\n"
                      f"wxPython {wx.__version__}",
                      "About Chip Helper",
                      wx.OK | wx.ICON_INFORMATION)

def set_monospace_font(window):
    """Set the font of a window to monospace"""
    font = window.Font
    font.Family = wx.FONTFAMILY_MODERN
    window.Font = font

class EasyModePanel(wx.Panel):
    """Tab of 'Easy Mode'"""
    BASIC_MASKS = [
        # 6 blocks, at most 3x3
        'xx/xxx/.x',
        'xx/.xx/xx',
        'xxx/xx/x',
        'xxx/xxx',
        # 6 blocks, at most 4x4
        'xxxx/x..x',
        '.x/xxxx/.x',
        'xxxx/.xx',
        'xxx/.xxx',
        '.xxx/xxx',
        # 6 blocks, at most 6x6
        'xxxxxx',
        # 5 blocks type I, at most 3x3
        '.x/xxx/x',
        'x/xxx/.x',
        'x/xxx/x',
        '.xx/xx/x',
        '.x/xxx/.x',
        # 5 blocks type I, at most 4x4
        'xx/.xxx',
        '.xxx/xx',
        'xxxx/.x',
        '.x/xxxx',
    ]

    MASK_COLOUR = wx.Colour(0xffd3d3d3)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # d, p, a, r are in number of blocks
        self._chip = Chip(Mask(self.BASIC_MASKS[0]))

        sizer = wx.BoxSizer(wx.VERTICAL)

        sizer.Add(self._create_mask_buttons())

        sizer.Add(self._create_option_line(), flag=wx.EXPAND)

        self._stat_buttons = []
        sizer.Add(self._create_stat_buttons())

        sizer.Add(self._create_text_line(), flag=wx.EXPAND)

        self.Sizer = sizer

        self._update()

    def _create_mask_buttons(self):
        sizer = wx.GridBagSizer()

        for i, basicmask in enumerate(self.BASIC_MASKS):
            for j, mask in enumerate(Mask(basicmask).rotations()):
                button = wx.Button(self, style=wx.BU_EXACTFIT)
                sizer.Add(button, pos=(j, i), flag=wx.EXPAND)
                button.Bitmap = self._create_bitmap(mask)

                self.Bind(wx.EVT_BUTTON, self._on_mask_button(mask), button)
        return sizer

    def _create_bitmap(self, mask, bsz=None):
        if bsz is None:
            bsz = 31 // max(mask.width(), mask.height())
        bitmap = wx.Bitmap(mask.width() * bsz + 1,
                           mask.height() * bsz + 1)
        dc = wx.MemoryDC()
        dc.SelectObject(bitmap)
        self._draw_mask(dc, mask, bsz)
        dc.SelectObject(wx.NullBitmap)

        bitmap.SetMaskColour(self.MASK_COLOUR)
        return bitmap

    def _draw_mask(self, dc, mask, bsz):
        dc.Background = wx.TheBrushList.FindOrCreateBrush(self.MASK_COLOUR)
        dc.Clear()

        dc.Pen = wx.BLACK_PEN
        dc.Brush = wx.WHITE_BRUSH
        for row, col in mask:
            dc.DrawRectangle(col * bsz, row * bsz, bsz + 1, bsz + 1)

    def _create_option_line(self):
        self._slider = wx.Slider(self, maxValue=20,
                                 style=wx.SL_LABELS | wx.SL_AUTOTICKS)
        self.Bind(wx.EVT_SLIDER, self._on_slider, self._slider)
        return self._slider

    def _create_stat_buttons(self):
        sizer = wx.GridSizer(cols=7)

        for stat in range(4):
            bitmap = wx.Bitmap(_ICON_PATHES[stat])
            sizer.Add(wx.StaticBitmap(self, bitmap=bitmap), flag=wx.EXPAND)

            for blocks in range(6):
                button = wx.ToggleButton(self, style=wx.BU_EXACTFIT)
                self._stat_buttons.append(button)
                sizer.Add(button, flag=wx.EXPAND)

                event_handler = self._on_stat_button(stat, blocks)
                self.Bind(wx.EVT_TOGGLEBUTTON, event_handler, button)

        return sizer

    def _create_text_line(self):
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        self._bt_value = wx.RadioButton(self, style=wx.RB_GROUP, label="Value")
        self._bt_value.Value = True
        sizer.Add(self._bt_value, flag=wx.ALIGN_CENTER)

        self._bt_blocks = wx.RadioButton(self, label="Blocks")
        sizer.Add(self._bt_blocks, flag=wx.ALIGN_CENTER)

        self._text = wx.TextCtrl(self, style=wx.TE_READONLY)
        sizer.Add(self._text, proportion=1)
        set_monospace_font(self._text)

        self.Bind(wx.EVT_RADIOBUTTON, self._on_radio_button, self._bt_value)
        self.Bind(wx.EVT_RADIOBUTTON, self._on_radio_button, self._bt_blocks)

        return sizer

    def _update(self):
        level = self._slider.Value

        for i, button in enumerate(self._stat_buttons):
            stat, blocks = i // 6, i % 6
            statcoef = [4.4, 12.7, 7.1, 5.7][stat]
            value = Chip.compute_value(statcoef, blocks, 1, level)
            button.Label = str(value)
            button.Value = blocks == getattr(self._chip, 'dpar'[stat])

        chip = self._chip
        if self._bt_value.Value:
            chip = chip.to_value(5, level)
        self._text.Value = str(chip)

    def _on_mask_button(self, mask):
        def on_mask_button(_event):
            self._chip.mask = mask
            self._update()
        return on_mask_button

    def _on_slider(self, _event):
        self._update()

    def _on_stat_button(self, stat, blocks):
        def on_stat_button(_event):
            setattr(self._chip, 'dpar'[stat], blocks)
            self._update()
        return on_stat_button

    def _on_radio_button(self, _event):
        self._update()

class ImportHycdesPanel(wx.Panel):
    """Import savecode from hycdes.com"""

    SHAPES = {
        '56': {
            '1': Mask('xx/xx/xx'),
            '2': Mask('xxx/xx/x'),
            '3': Mask('xxxx/.xx'),
            '41': Mask('xxx/.xxx'),
            '42': Mask('.xxx/xxx'),
            '5': Mask('xx/.xx/xx'),
            '6': Mask('.x/xxx/.x/.x'),
            '7': Mask('x/x/x/x/x/x'),
            '8': Mask('xxxx/x..x'),
            '9': Mask('xx/xxx/.x'),
        },
        '551': {
            '11': Mask('.xx/xx/.x'),
            '12': Mask('xx/.xx/.x'),
            '21': Mask('xxx/..xx'),
            '22': Mask('.xxx/xx'),
            '31': Mask('.x/xx/.x/.x'),
            '32': Mask('x/xx/x/x'),
            '4': Mask('xxx/.x/.x'),
            '5': Mask('.xx/xx/x'),
            '6': Mask('.x/xxx/.x'),
            '81': Mask('x/xx/xx'),
            '82': Mask('.x/xx/xx'),
            '9': Mask('x/x/x/x/x'),
            '10': Mask('xx/x/xx'),
            '111': Mask('xx/.x/.xx'),
            '112': Mask('.xx/.x/xx'),
            '120': Mask('xxx/x/x'),
            '131': Mask('x/x/x/xx'),
            '132': Mask('.x/.x/.x/xx'),
        }
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        self._text_in = wx.TextCtrl(
            self, style=wx.TE_MULTILINE | wx.TE_CHARWRAP,
            value='Paste savecode here')
        sizer.Add(self._text_in, proportion=1, flag=wx.EXPAND)
        set_monospace_font(self._text_in)

        button = wx.Button(self, id=wx.ID_CONVERT, label='>>')
        sizer.Add(button, flag=wx.ALIGN_CENTER)
        self.Bind(wx.EVT_BUTTON, self._on_button, button)

        self._text_out = wx.TextCtrl(
            self, style=wx.TE_MULTILINE | wx.TE_READONLY,
            value='Output will be here\n'
                  'Note that it may not be the correct rotation')
        sizer.Add(self._text_out, proportion=1, flag=wx.EXPAND)
        set_monospace_font(self._text_out)

        self.Sizer = sizer

    @staticmethod
    def _convert(hycdes):
        hycdes = hycdes[hycdes.index('!') + 1 : hycdes.index('?')]
        hycdes = hycdes.rstrip('&').split('&')

        lines = []
        for hline in hycdes:
            _, _, clazz, shape, level, acc, rel, dmg, pen, _ = hline.split(',')

            try:
                mask = ImportHycdesPanel.SHAPES[clazz][shape]
            except KeyError:
                raise ValueError(f'Unknown classNum {clazz} and/or typeNum {shape}')

            chip = Chip(mask, int(dmg), int(pen), int(acc), int(rel))
            chip = chip.to_value(5, int(level))

            lines.append(str(chip))

        return '\n'.join(lines)

    def _on_button(self, _event):
        try:
            self._text_out.Value = self._convert(self._text_in.Value)
        except ValueError:
            wx.MessageBox("Invalid savecode", style=wx.OK | wx.ICON_ERROR)

class ImportExcelPanel(wx.Panel):
    """Import from an Excel workbook"""

    DEFAULT_SHEET_NAME = '\u82af\u7247\u603b\u8868'

    SHAPES = {
        '1': Mask('xxx/xxx'),
        '2': Mask('xxx/xx/x'),
        '3': Mask('xxxx/.xx'),
        '4a': Mask('xxx/.xxx'),
        '4b': Mask('.xxx/xxx'),
        '5': Mask('xx/.xx/xx'),
        '6': Mask('..x/xxxx/..x'),
        '7': Mask('xxxxxx'),
        '8': Mask('xxxx/x..x'),
        '9': Mask('xx/xxx/.x'),
        'Fa': Mask('.xx/xx/.x'),
        'Fb': Mask('xx/.xx/.x'),
        'Na': Mask('xxx/..xx'),
        'Nb': Mask('.xxx/xx'),
        'T': Mask('x/xxx/x'),
        'W': Mask('..x/.xx/xx'),
        'X': Mask('.x/xxx/.x'),
        'Ya': Mask('..x/xxxx'),
        'Yb': Mask('.x/xxxx'),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._wb_path = None

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        self._choice = None
        self._shape = None
        self._stats = None

        sizer.Add(self._create_main_panel(), proportion=1, flag=wx.EXPAND)

        self._button = wx.Button(self, id=wx.ID_CONVERT, label='>>')
        sizer.Add(self._button, flag=wx.ALIGN_CENTER)
        self.Bind(wx.EVT_BUTTON, self._on_button, self._button)

        self._text_out = wx.TextCtrl(
            self, style=wx.TE_MULTILINE | wx.TE_READONLY,
            value='Output will be here\n'
                  'Note that it may not be the correct rotation')
        sizer.Add(self._text_out, proportion=1, flag=wx.EXPAND)
        set_monospace_font(self._text_out)

        self._enable_controls(False)

        self.Sizer = sizer

    def _enable_controls(self, enabled=True):
        self._button.Enable(enabled)
        self._choice.Enable(enabled)
        self._shape.Enable(enabled)
        for stat in self._stats:
            stat.Enable(enabled)

    def _create_main_panel(self):
        sizer = wx.BoxSizer(wx.VERTICAL)

        file_picker = wx.FilePickerCtrl(self, wildcard=self._get_wildcard())
        sizer.Add(file_picker, flag=wx.EXPAND)

        sizer.Add(self._create_option_panel(), flag=wx.EXPAND)

        self.Bind(wx.EVT_FILEPICKER_CHANGED, self._on_file_open, file_picker)

        return sizer

    def _create_option_panel(self):
        sizer = wx.FlexGridSizer(cols=2)

        # Worksheet
        sizer.Add(wx.StaticText(self, label="Worksheet"), flag=wx.ALIGN_CENTER)

        self._choice = wx.Choice(self)
        sizer.Add(self._choice, flag=wx.ALIGN_CENTER | wx.EXPAND)

        # Shape
        sizer.Add(wx.StaticText(self, label="Shape"), flag=wx.ALIGN_CENTER)

        self._shape = wx.TextCtrl(self)
        sizer.Add(self._shape, flag=wx.ALIGN_CENTER | wx.EXPAND)
        self._shape.Hint = "B11:B310"

        # stats
        self._stats = []
        for stat in range(4):
            bitmap = wx.Bitmap(_ICON_PATHES[stat])
            sizer.Add(wx.StaticBitmap(self, bitmap=bitmap),
                      flag=wx.ALIGN_CENTER)

            stat_text = wx.TextCtrl(self)
            self._stats.append(stat_text)
            sizer.Add(stat_text, flag=wx.ALIGN_CENTER | wx.EXPAND)

            col = chr(ord('H') + stat)
            stat_text.Hint = f'{col}11:{col}310'

        return sizer

    def _on_file_open(self, event):
        self._wb_path = event.Path
        try:
            workbook = openpyxl.load_workbook(self._wb_path, read_only=True,
                                              data_only=True)
        except Exception as exc:
            wx.MessageBox(str(exc), style=wx.ICON_ERROR)
            return

        self._enable_controls()
        self._choice.Items = workbook.sheetnames
        self._choice.StringSelection = self.DEFAULT_SHEET_NAME
        self.Layout()

    @staticmethod
    def _get_range(worksheet, textctrl):
        text = textctrl.Value or textctrl.Hint

        try:
            return [cell.value for row in worksheet[text] for cell in row]
        except (ValueError, AttributeError, TypeError):
            raise ValueError(f'Invalid range: {text!r}')

    def _on_button(self, _event):
        if self._choice.Selection == wx.NOT_FOUND:
            wx.MessageBox('Please select a worksheet', style=wx.ICON_ERROR)
            return

        workbook = openpyxl.load_workbook(self._wb_path, read_only=True,
                                          data_only=True)
        worksheet = workbook[self._choice.StringSelection]

        try:
            all_shapes = self._get_range(worksheet, self._shape)
            all_stats = [self._get_range(worksheet, stat) for stat in self._stats]
        except ValueError as exc:
            wx.MessageBox(str(exc), style=wx.ICON_ERROR)
            return

        lines = []
        for shape, *stats in zip(all_shapes, *all_stats):
            if not shape:
                continue
            try:
                mask = self.SHAPES[str(shape)]
            except KeyError:
                wx.MessageBox(f'Unknown shape: {shape!r}', style=wx.ICON_ERROR)
                return

            lines.append(str(Chip(mask, *stats)))

        self._text_out.Value = '\n'.join(lines)

    @staticmethod
    def _get_wildcard():
        excel = '*.xlsx;*.xlsm;*.xltx;*.xltm'
        return f'Excel workbooks ({excel})|{excel}'

class StrengthenPanel(wx.Panel):
    """Panel that can strengthen chips"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._stars = None
        self._text_in = None
        self._slider = None
        self._text_out = None

        sizer = wx.BoxSizer(wx.HORIZONTAL)

        sizer.Add(self._create_left_panel(), proportion=1, flag=wx.EXPAND)

        button = wx.Button(self, id=wx.ID_CONVERT, label='>>')
        sizer.Add(button, flag=wx.ALIGN_CENTER)
        self.Bind(wx.EVT_BUTTON, self._on_button, button)

        sizer.Add(self._create_right_panel(), proportion=1, flag=wx.EXPAND)

        self.Sizer = sizer

    def _create_left_panel(self):
        sizer = wx.BoxSizer(wx.VERTICAL)

        sizer.Add(self._create_stars_panel(), flag=wx.EXPAND)

        self._text_in = wx.TextCtrl(
            self, style=wx.TE_MULTILINE | wx.TE_CHARWRAP,
            value='Paste chips here')
        sizer.Add(self._text_in, proportion=1, flag=wx.EXPAND)
        set_monospace_font(self._text_in)

        return sizer

    def _create_stars_panel(self):
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        self._stars = []
        for i in range(5, 1, -1):
            style = wx.RB_GROUP if i == 5 else 0
            button = wx.RadioButton(self, style=style, label="\u2605" * i)
            self._stars.append(button)
            sizer.Add(button)

        return sizer

    def _create_right_panel(self):
        sizer = wx.BoxSizer(wx.VERTICAL)

        self._slider = wx.Slider(self, value=20, maxValue=20,
                                 style=wx.SL_LABELS | wx.SL_AUTOTICKS)
        sizer.Add(self._slider, flag=wx.EXPAND)

        self._text_out = wx.TextCtrl(
            self, style=wx.TE_MULTILINE | wx.TE_READONLY,
            value='Output will be here')
        sizer.Add(self._text_out, proportion=1, flag=wx.EXPAND)
        set_monospace_font(self._text_out)

        return sizer

    def _on_button(self, _event):
        chips_text = self._text_in.Value
        try:
            all_chips = Parameter.parse_chips(chips_text, omit_section=True)
        except Exception:
            wx.MessageBox('Invalid config', style=wx.ICON_ERROR)
            return

        stars = [5 - i for i, button in enumerate(self._stars) if button.Value]
        if not stars:
            wx.MessageBox('Please select number of stars', style=wx.ICON_ERROR)
            return
        stars = stars[0]

        level = self._slider.Value

        lines = []
        bad_chips = 0
        for section, chips in all_chips.items():
            if section:
                lines.append(f'[{section}]')
            for chip in chips:
                try:
                    chip = chip.to_blocks(stars).to_value(stars, level)
                except ValueError:
                    bad_chips += 1
                    lines.append('; ' + str(chip))
                else:
                    lines.append(str(chip))

        self._text_out.Value = '\n'.join(lines)

        if bad_chips:
            wx.MessageBox(f'{bad_chips} invalid chip(s) found',
                          style=wx.ICON_WARNING)

def main():
    """The main function, of course"""
    app = wx.App()
    frame = ChipHelperFrame(None, title="Chip Helper")

    frame.Show()
    app.MainLoop()

if __name__ == '__main__':
    main()
