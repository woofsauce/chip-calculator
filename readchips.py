#!/usr/bin/env python3
"""Read chips from images"""
from dataclasses import replace
import collections
import sys

import cv2
import numpy
import wx

from chiplib import Mask, Chip

class Matcher:
    """The algorithm to find chip in images"""

    ICON_SIZE = 32
    SAME_CHIP_THRE = 10000
    CHIP_X = 3
    CHIP_Y = 140
    CHIP_W = 148
    CHIP_H = 180

    TEXT_BG = (cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 255, 255), 6)
    TEXT_OK = (cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 127, 0), 2)
    TEXT_ERR = (cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 0, 255), 2)

    def __init__(self):
        self._icons = {}
        self._digits = {}

    def add_icon(self, label, img):
        """Add an icon of stat."""
        self._icons[label] = img

    def add_digit(self, label, holes, img):
        """Add an icon of digit."""
        self._digits[label] = holes, img

    @staticmethod
    def find_squares(img):
        """Find some squares on the image."""
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        _, img = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)
        cons, _ = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        for con in cons:
            conlen = cv2.arcLength(con, True)
            con = cv2.approxPolyDP(con, 0.02 * conlen, True)
            if len(con) != 4:
                # not a quadrilaterals
                continue

            x, y, w, h = cv2.boundingRect(con)
            if w <= 10 or h <= 10:
                # Too small
                continue
            if not -1 <= w - h <= 1:
                # bounding box not a square
                continue

            yield x, y, w

    @staticmethod
    def detect_icon_size(img):
        """Find length of sides of the icons on the image."""
        counter = collections.Counter(w for _, _, w in Matcher.find_squares(img))
        if not counter:
            raise ValueError('No icon detected')
        return counter.most_common(1)[0][0] - 1

    @staticmethod
    def resize(img):
        """Resize image automatically."""
        iconsz = Matcher.detect_icon_size(img)
        if not -1 <= iconsz - Matcher.ICON_SIZE <= 1:
            scale = Matcher.ICON_SIZE / iconsz
            img = cv2.resize(img, (0, 0), fx=scale, fy=scale)
        return img

    @staticmethod
    def _cc_argmax(score, comps, n):
        best = [None] * n
        for i, j in numpy.transpose(comps.nonzero()):
            x = comps[i, j]
            if best[x] is None or score[i, j] > score[best[x]]:
                best[x] = i, j
        return best

    def find_icons(self, img):
        """Find icons on the image."""
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        for label, icon in self._icons.items():
            match = cv2.matchTemplate(icon, img, cv2.TM_CCOEFF_NORMED)

            _, mat = cv2.threshold(match, 0.7, 255, cv2.THRESH_BINARY)
            n, comps = cv2.connectedComponents(mat.astype(numpy.uint8))

            best = self._cc_argmax(match, comps, n)

            for index in best[1:]:
                yield index, label

    @staticmethod
    def _icon_near(icon1, icon2):
        (y1, x1), _ = icon1
        (y2, x2), _ = icon2
        return (y1 - y2) ** 2 + (x1 - x2) ** 2 <= Matcher.SAME_CHIP_THRE

    @staticmethod
    def group_icons(icons):
        """Group icons by chip."""
        icons = list(icons)
        while icons:
            center = icons[0]
            group = [i for i in icons if Matcher._icon_near(i, center)]
            icons = [i for i in icons if not Matcher._icon_near(i, center)]
            yield group

    @staticmethod
    def icons_to_chip(icons):
        """Get location of a chip from its icons."""
        minx = min(x for (_, x), _ in icons)
        maxy = max(y for (y, _), _ in icons)
        return (minx - Matcher.CHIP_X, maxy - Matcher.CHIP_Y,
                Matcher.CHIP_W, Matcher.CHIP_H)

    def find_chips(self, img):
        """Find all (locations of) chips in the image."""
        img_h, img_w = img.shape[:2]
        for group in self.group_icons(self.find_icons(img)):
            chip = self.icons_to_chip(group)
            x, y, w, h = chip
            if x >= 0 and x + w <= img_w and y >= 0 and y + h <= img_h:
                yield chip, group

    @staticmethod
    def _mask_contour_key(img):
        x, y = img.shape
        x /= 2
        y /= 2
        def key(con):
            x1, y1, w, h = cv2.boundingRect(con)
            x2, y2 = x1 + w, y1 + h
            return min(x - x1, x2 - x, y - y1, y2 - y)
        return key

    @staticmethod
    def _trig(img, n):
        h, w = img.shape[:2]
        return ([h - 1 - i for i in range(n) for j in range(n - i)],
                [w - 1 - j for i in range(n) for j in range(n - i)])

    @staticmethod
    def _contains_center(rect, imgshape):
        x, y, w, h = rect
        img_h, img_w = imgshape
        return x * 2 <= img_w <= (x + w) * 2 and y * 2 <= img_h <= (y + h) * 2

    @staticmethod
    def _is_uturn(p1, p2, p3):
        return numpy.inner(p2 - p1, p2 - p3) > 0

    @staticmethod
    def _get_block_size(con):
        def key(x):
            return numpy.minimum(con % x, -con % x).min(axis=1).max()
        return min(range(20, 30), key=key)

    @staticmethod
    def find_mask_outline(img):
        """Find outline of the mask on a chip"""

        img = cv2.bilateralFilter(img, 5, 75, 75)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.inRange(img, 128, 160)
        img[Matcher._trig(img, 46)] = 0
        img[:32, -32:] = 0

        cons, _ = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        con = max(cons, key=cv2.contourArea)
        con = con.reshape((-1, 2))

        shift = con.min(axis=0)
        con -= shift

        bsz = Matcher._get_block_size(con)
        con = (con + bsz // 2) // bsz

        # remove duplicate adjacent points
        con = con[(con != numpy.roll(con, -1, axis=0)).any(axis=-1)]

        if not cv2.contourArea(con):
            uturns = []
            for i in range(len(con)):
                if Matcher._is_uturn(con[i - 2], con[i - 1], con[i]):
                    uturns.append((i - 1) % len(con))
            con = con[min(uturns) : max(uturns) + 1]

        return con

    @staticmethod
    def outline_to_mask(con):
        """Convert outline to mask"""
        w, h = con.max(axis=0)
        grid = numpy.zeros((h, w))

        for x1, y1, x2, y2 in numpy.c_[con, numpy.roll(con, -1, axis=0)]:
            if y1 == y2 and y1 < h:
                if x1 < x2:
                    grid[y1, x1 : x2] += 1
                elif x1 > x2:
                    grid[y1, x2 : x1] -= 1

        grid = numpy.cumsum(grid, axis=0)
        return Mask([(r, c) for r, c in numpy.transpose(grid.nonzero())])

    @staticmethod
    def find_mask(img):
        """Find the mask on a chip"""
        con = Matcher.find_mask_outline(img)
        return Matcher.outline_to_mask(con)

    def img_to_digit(self, img):
        """Recognize the digit on the image"""
        holes = cv2.connectedComponents(~img)[0] - 2

        best = bestscore = None
        for digit, (expected_holes, template) in self._digits.items():
            if holes != expected_holes:
                continue
            score = cv2.matchTemplate(template, img, cv2.TM_SQDIFF_NORMED).min()

            if best is None or score < bestscore:
                best = digit
                bestscore = score

        return best

    def img_to_int(self, img):
        """Recognize the integer on the image"""
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        _, img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

        n, comps = cv2.connectedComponents(img)
        border = numpy.concatenate((comps[[0, -1]].flatten(), comps[:, [0, -1]].flatten()))
        border = numpy.unique(border)

        pieces = []
        for i in range(n):
            if i in border:
                continue
            piece = (comps == i) * numpy.uint8(255)
            if numpy.sum(piece) < 10 * 255:
                continue
            x = piece.any(axis=0).nonzero()[0][0]
            pieces.append((x, piece))
        pieces.sort(key=lambda x: x[0])

        ans = 0
        for _, piece in pieces:
            ans = ans * 10 + self.img_to_digit(piece)
        return ans

    def recognize_attrs(self, img, group):
        """Recognize value of each attribute in group"""
        for (y, x), label in group:
            value = self.img_to_int(img[y + 8 : y + 36, x + 32 : x + 72])
            yield label, value

    def recognize_chips(self, img):
        """Recognize the chips on the image"""

        for (x, y, w, _), group in self.find_chips(img):
            h = 110 if len(group) > 2 else 144
            mask = self.find_mask(img[y : y + h, x : x + w])
            attrs = dict(self.recognize_attrs(img, group))

            valid, chip = self.check_chip(Chip(mask, **attrs))
            yield chip, valid, (x, y)

    @staticmethod
    def check_chip(chip):
        """Check and attempts to correct the chip"""
        if not 3 <= len(chip.mask) <= 6:
            return False, chip

        for stars in range(2, 6):
            try:
                chip.to_blocks(stars)
            except ValueError:
                continue
            return True, chip

        if chip.mask == Mask('x/x/x/x/x'):
            newchip = replace(chip, mask=Mask('x/x/x/x/x/x'))
            ok, _ = Matcher.check_chip(newchip)
            if ok:
                return True, newchip

        if chip.mask == Mask('xxxxx'):
            newchip = replace(chip, mask=Mask('xxxxxx'))
            ok, _ = Matcher.check_chip(newchip)
            if ok:
                return True, newchip

        return False, chip

    def annotate(self, img, pos, chip, valid=True):
        """Draw the chip on the image"""
        x, y = pos
        style = self.TEXT_OK if valid else self.TEXT_ERR

        cv2.putText(img, str(chip.mask), (x, y), *self.TEXT_BG)
        cv2.putText(img, str(chip.mask), (x, y), *style)

        for label in self._icons:
            value = getattr(chip, label, None)
            if not value:
                continue

            y += 25
            cv2.putText(img, f'{label}={value}', (x, y), *self.TEXT_BG)
            cv2.putText(img, f'{label}={value}', (x, y), *style)

    def annotate_all(self, img, path, chips):
        """Draw tons of information on the image"""
        height = len(img)
        allvalid = all(valid for _, valid, _ in chips)
        style = self.TEXT_OK if allvalid else self.TEXT_ERR
        cv2.putText(img, path, (0, height - 10), *self.TEXT_BG)
        cv2.putText(img, path, (0, height - 10), *style)

        for chip, valid, (x, y) in chips:
            self.annotate(img, (x, y), chip, valid)

def create_matcher():
    """Create a Matcher and load icons"""
    matcher = Matcher()
    for stat in 'dpar':
        img = cv2.imread(f'icons/{stat}.png', cv2.IMREAD_GRAYSCALE)
        matcher.add_icon(stat, img)
    for i, holes in zip(range(10), [2, 0, 0, 0, 1, 0, 1, 0, 2, 1, 0]):
        img = cv2.imread(f'icons/{i}.png', cv2.IMREAD_GRAYSCALE)
        matcher.add_digit(i, holes, img)
    return matcher

def cli_main():
    """Main function without gui"""
    matcher = create_matcher()

    nchips = 0
    for imgpath in sys.argv[1:]:
        img = cv2.imread(imgpath)
        if img is None:
            print(f'Cannot read image {imgpath}', file=sys.stderr)
            continue
        try:
            img = matcher.resize(img)
        except ValueError:
            print(f'Cannot find chips in {imgpath}', file=sys.stderr)
            continue

        chips = list(matcher.recognize_chips(img))
        if not chips:
            print(f'Cannot find chips in {imgpath}', file=sys.stderr)
            continue

        print(f'; ' + imgpath)
        for chip, valid, (x, y) in chips:
            matcher.annotate(img, (x, y), chip, valid)
            print(('' if valid else '; ') + str(chip))
            nchips += valid
        print()
    return 0 if nchips > 0 else 1

def set_monospace_font(window):
    """Set the font of a window to monospace

    Copied from chiphelper"""
    font = window.Font
    font.Family = wx.FONTFAMILY_MODERN
    window.Font = font

class BitmapDialog(wx.Dialog):
    """A dialog displaying bitmap"""
    def __init__(self, parent, bitmap, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(wx.StaticBitmap(self, bitmap=bitmap))
        sizer.Add(self.CreateStdDialogButtonSizer(wx.OK))
        sizer.SetSizeHints(self)
        self.Sizer = sizer

class ChipReaderFrame(wx.Frame):
    """Main frame of Chip Reader"""
    def __init__(self, parent, matcher, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)

        self._make_menu_bar()

        self._matcher = matcher
        self._open_dialog = wx.FileDialog(
            self, style=wx.FD_OPEN | wx.FD_MULTIPLE | wx.FD_FILE_MUST_EXIST,
            wildcard=ChipReaderFrame._get_open_wildcard())
        self._save_dialog = wx.FileDialog(
            self, style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT,
            wildcard=ChipReaderFrame._get_save_wildcard())
        self._save_path = None

        self._text = wx.TextCtrl(
            self, style=wx.TE_MULTILINE | wx.TE_CHARWRAP,
            value='; Found chips will be appended here\n')
        set_monospace_font(self._text)

    @staticmethod
    def _get_open_wildcard():
        extensions = [
            'bmp', 'dib', 'jpeg', 'jpg', 'jpe', 'jp2', 'png', 'webp',
            'pbm', 'pgm', 'ppm', 'pxm', 'pnm', 'pfm', 'sr', 'ras',
            'tiff', 'tif', 'exr', 'hdr', 'pic',
        ]
        image = ';'.join(f'*.{ext}' for ext in extensions)

        return f'Image files|{image}|All files|*.*'

    @staticmethod
    def _get_save_wildcard():
        txt = '*.txt'
        return f'Text files ({txt})|{txt}|All files|*.*'

    def _make_menu_bar(self):
        file_menu = wx.Menu()
        open_item = file_menu.Append(wx.ID_OPEN)
        save_item = file_menu.Append(wx.ID_SAVE)
        saveas_item = file_menu.Append(wx.ID_SAVEAS)
        file_menu.AppendSeparator()
        exit_item = file_menu.Append(wx.ID_EXIT)

        help_menu = wx.Menu()
        about_item = help_menu.Append(wx.ID_ABOUT)

        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, "&File")
        menu_bar.Append(help_menu, "&Help")

        self.MenuBar = menu_bar

        self.Bind(wx.EVT_MENU, self._on_open, open_item)
        self.Bind(wx.EVT_MENU, self._on_save, save_item)
        self.Bind(wx.EVT_MENU, self._on_saveas, saveas_item)
        self.Bind(wx.EVT_MENU, self._on_exit, exit_item)
        self.Bind(wx.EVT_MENU, self._on_about, about_item)

    @staticmethod
    def _chips_to_text(path, chips):
        lines = [f'; {path}\n']
        for chip, valid, _ in chips:
            lines.append(f'{chip}\n' if valid else f'; {chip}\n')
        lines.append('\n')
        return ''.join(lines)

    def _img_to_bitmap(self, img):
        height, width, _ = img.shape

        display = wx.Display(wx.Display.GetFromWindow(self))
        _, _, maxw, maxh = display.ClientArea
        if width > maxw or height > maxh:
            scale = min(maxw / width, maxh / height) * 0.9
            img = cv2.resize(img, (0, 0), fx=scale, fy=scale)
            height, width, _ = img.shape

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        return wx.Bitmap.FromBuffer(width, height, img)

    def _open(self, path):
        img = cv2.imread(path)
        if img is None:
            wx.MessageBox(f'Cannot read image {path}', style=wx.ICON_ERROR)
            return

        matcher = self._matcher
        try:
            img = matcher.resize(img)
        except ValueError:
            wx.MessageBox(f'Cannot find chips in {path}', style=wx.ICON_ERROR)
            return

        chips = list(matcher.recognize_chips(img))
        if not chips:
            wx.MessageBox(f'Cannot find chips in {path}', style=wx.ICON_ERROR)
            return

        self._text.Value += self._chips_to_text(path, chips)
        matcher.annotate_all(img, path, chips)
        bitmap = self._img_to_bitmap(img)

        with BitmapDialog(self, bitmap, title=path) as dlg:
            dlg.ShowModal()

    def _save(self):
        open(self._save_path, 'w').write(self._text.Value)

    def _saveas(self):
        if self._save_dialog.ShowModal() == wx.ID_OK:
            self._save_path = self._save_dialog.Path
            self._save()

    def _on_open(self, _event):
        if self._open_dialog.ShowModal() == wx.ID_OK:
            for path in self._open_dialog.Paths:
                self._open(path)

    def _on_save(self, _event):
        if self._save_path is None:
            self._saveas()
        else:
            self._save()

    def _on_saveas(self, _event):
        self._saveas()

    def _on_exit(self, _event):
        self.Close(True)

    @staticmethod
    def _on_about(_event):
        wx.MessageBox("Copyright \u00a9 2019 Pochang Chen\n\n"
                      "This software is under the MIT License.\n"
                      "See LICENSE.txt for details.\n\n"
                      "Library versions:\n"
                      f"OpenCV {cv2.__version__}\n"
                      f"NumPy {numpy.__version__}\n"
                      f"wxPython {wx.__version__}",
                      "About Chip Reader",
                      wx.OK | wx.ICON_INFORMATION)

def main():
    """Main function"""
    if len(sys.argv) > 1:
        return cli_main()

    matcher = create_matcher()

    app = wx.App()
    frame = ChipReaderFrame(None, matcher, title="Chip Reader")

    frame.Show()
    app.MainLoop()
    return 0

if __name__ == '__main__':
    sys.exit(main())
